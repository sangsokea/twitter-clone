/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['img.freepik.com', 'help.twitter.com'],
  },
}

module.exports = nextConfig
