import Post from "@/components/Post";
import Sidebar from "@/components/Sidebar";
import { ArrowLeft } from "@/components/icons/Icons";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { db } from "../../../firebase";
import { collection, doc, onSnapshot, orderBy, query } from "firebase/firestore";
import { AnimationPresence, motion } from "framer-motion";
import Comment from "@/components/Comment";
import Widgets from "@/components/Widgets";
import CommentModal from "@/components/CommentModal";

export default function PostPage({ newsResults, randomUsersResult }) {
  const router = useRouter();
  const { id } = router.query;
  const [post, setPost] = useState();
  const [comments, setComments] = useState([]);

  // get post from firebase
    useEffect(() => {
    onSnapshot(doc(db, "posts", id), (snapshot) => {
        setPost(snapshot);
        });
    }, [db, id]);

  // get comments from firebase
  useEffect(() => {
    onSnapshot(
      query(
        collection(db, "posts", id, "comments"),
        orderBy("timestamp", "desc")
      ),
      (snapshot) => setComments(snapshot.docs)
    );
  }, [db, id]);

  console.log("post: ", post);
  return (
    <div>
      <main className="flex min-h-screen max-auto">
        {/* Side bar */}
        <Sidebar />

        {/* Feed */}
        <div className="xl:ml-[370px] border-l border-r border-gray-200  xl:min-w-[576px] sm:ml-[73px] flex-grow max-w-xl">
          <div className="flex items-center space-x-2  py-2 px-3 sticky top-0 z-50 bg-white border-b border-gray-200">
            <div onClick={() => router.push("/")} className="hoverEffect">
              <ArrowLeft className="h-5" />
            </div>
            <h2 className="text-lg sm:text-xl font-bold cursor-pointer">
              Tweet
            </h2>
          </div>

          {/* Post */}
          {post && <Post key={post.id} post={post} id={post.id} />}
          {/* Comments */}
          {comments.length > 0 && (
            <div className="">
              <AnimationPresence>
                {comments.map((comment) => (
                  <motion.div
                    key={comment.id}
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    exit={{ opacity: 0 }}
                    transition={{ duration: 1 }}
                  >
                    <Comment
                      key={comment.id}
                      commentId={comment.id}
                      originalPostId={id}
                      comment={comment.data()}
                    />
                  </motion.div>
                ))}
              </AnimationPresence>
            </div>
          )}
        </div>
        {/* Widgets */}

        <Widgets
          newsResults={newsResults.articles}
          randomUsersResults={randomUsersResult.results}
        />

        {/* Modal */}

        <CommentModal />
      </main>
    </div>
  );
}

// https://saurav.tech/NewsAPI/top-headlines/category/business/us.json

export async function getServerSideProps() {
  const newsResults = await fetch(
    `https://saurav.tech/NewsAPI/top-headlines/category/business/us.json`
  )
    .then((res) => res.json())
    .catch((err) => console.log("Get news error: ", err));

  // Who to follow section

  let randomUsersResult = [];

  try {
    const res = await fetch(
      "https://randomuser.me/api/?results=30&inc=name,login,picture"
    );
    randomUsersResult = await res.json();
  } catch (err) {
    console.log("Get random users error: ", err);
  }

  return {
    props: {
      newsResults,
      randomUsersResult: randomUsersResult.results,
    },
  };
}
