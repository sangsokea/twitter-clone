import Modal from "react-modal";
import { modalState, postIdState } from "@/atom/modalAtom";
import { userState } from "@/atom/userAtom";
import {
  addDoc,
  collection,
  doc,
  onSnapshot,
  serverTimestamp,
} from "firebase/firestore";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useRecoilState } from "recoil";
import { db } from "../../firebase";
import { EmojiHappy, Photograph, XIcon } from "./icons/Icons";
import Moment from "react-moment";

export default function CommentModal() {
    const [open, setOpen] = useRecoilState(modalState);
    const [postId] = useRecoilState(postIdState);
    const [currentUser] = useRecoilState(userState);
    const [post, setPost] = useState({});
    const [input, setInput] = useState("");
    const router = useRouter()


    useEffect(() => {
        onSnapshot(doc(db, "posts", postId), (snapshot) => {
          setPost(snapshot);
        });
      }, [postId, db]);
    
      async function sendComment() {
        await addDoc(collection(db, "posts", postId, "comments"), {
          comment: input,
          name: currentUser.name,
          username: currentUser.username,
          avatar: currentUser.avatar,
          timestamp: serverTimestamp(),
          userId: currentUser.uid,
        });
    
        setOpen(false);
        setInput("");
        router.push(`/posts/${postId}`);
      }
  return (
    <div>
      {open && (
        <Modal
          isOpen={open}
          onRequestClose={() => setOpen(false)}
          className="max-w-lg w-[90%]  absolute top-24 left-[50%] translate-x-[-50%] bg-white border-2 border-gray-200 rounded-xl shadow-md"
        >
          <div className="p-1">
            <div className="border-b border-gray-200 py-2 px-1.5">
              <div
                onClick={() => setOpen(false)}
                className="hoverEffect w-10 h-10 flex items-center justify-center"
              >
                <XIcon className="h-[23px] text-gray-700 p-0" />
              </div>
            </div>

            <div className="p-2 flex items-center space-x-1 relative">
              <span className="w-0.5 h-full z-[-1] absolute left-8 top-11 bg-gray-300" />
              <img
                className="h-11 w-11 rounded-full mr-4"
                src={post?.data()?.avatar}
                alt="user-img"
              />
              <h4 className="font-bold text-[15px] sm:text-[16px] hover:underline">
                {"hello"}
              </h4>
              <span className="text-sm sm:text-[15px]">
              @{post?.data()?.username} -{" "}
              </span>
              <span className="text-sm sm:text-[15px] hover:underline">
                <Moment fromNow>{post?.data()?.timestamp?.toDate()}</Moment>
              </span>
            </div>
            <p className="text-gray-500 text-[15px] sm:text-[16px] ml-16 mb-2">
            {post?.data()?.text}

            </p>

            <div className="flex  p-3 space-x-3">
              <img
                src={currentUser?.avatar}
                alt="user-img"
                className="h-11 w-11 rounded-full cursor-pointer hover:brightness-95"
              />
              <div className="flex items-center justify-between pt-2.5">
                <div className="flex">
                  <div>
                    <Photograph className="h-10 w-10 hoverEffect p-2 text-sky-500 hover:bg-sky-100" />
                    <EmojiHappy className="h-10 w-10 hoverEffect p-2 text-sky-500 hover:bg-sky-100" />
                  </div>
                  <button
                    onClick={sendComment}
                    disabled={!input.trim()}
                    className="bg-blue-400 text-white px-4 py-1.5 rounded-full font-bold shadow-md hover:brightness-95 disabled:opacity-50"
                  >
                    Reply
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      )}
    </div>
  );
}
