import React, { useEffect } from "react";
import Image from "next/image";
import SidebarMenuItem from "./SidebarMenuItem";

import {
  HomeIconSolid,
  HastTagOutline,
  BookmarkOutline,
  ListOutline,
  MessengerOutline,
  MoreOutline,
  ProfileOutline,
  NotificationOutline,
  MoreSolid,
  DotHorizontal,
} from "./icons/Icons";
import { useSession, signIn, signOut } from "next-auth/react";
import { userState } from "@/atom/userAtom";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { doc, getDoc } from "firebase/firestore";
import { db } from "../../firebase";
import { useRouter } from "next/router";
import { useRecoilState } from "recoil";

export default function Sidebar() {
  const router = useRouter();
  const [currentUser, setCurrentUser] = useRecoilState(userState); // user atom
  const auth = getAuth();



  // get user data from firestore
  useEffect(() => {
    console.log("Current User: ", currentUser);
    // listen for auth state changes
    onAuthStateChanged(auth, (user) => {
      if (user) {
        const fetchUser = async () => {
          // doc() method is used to get the document reference
          const docRef = doc(db, "users", auth.currentUser.providerData[0].uid);
          // getDoc() method is used to get the document snapshot
          const docSnap = await getDoc(docRef);
          if (docSnap.exists()) {
            setCurrentUser(docSnap.data());
          } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
          }
        };
        fetchUser();
      } else {
        setCurrentUser(null);
      }
    });
  }, []);

  // sign out
  const handleSignOut = () => {
    signOut(auth);
    setCurrentUser(null);
  };

  return (
    <div className="hidden sm:flex flex-col p-2 xl:items-start fixed h-full xl:ml-24">
      {/* Twitter Logo */}
      <div className="hoverEffect p-0 hover:bg-blue-100 xl:px-1">
        <Image
          src="https://help.twitter.com/content/dam/help-twitter/brand/logo.png"
          width={50}
          height={50}
          alt="twitter logo"
        />
      </div>

      {/* Menu */}
      <div className="mt-4 mb-2.5 xl:items-start">
        <SidebarMenuItem text="Home" Icon={HomeIconSolid} active={true} />
        <SidebarMenuItem text="Explore" Icon={HastTagOutline} />
        {currentUser && (
          <>
            <SidebarMenuItem text="Notification" Icon={NotificationOutline} />
            <SidebarMenuItem text="Messages" Icon={MessengerOutline} />
            <SidebarMenuItem text="Bookmark" Icon={BookmarkOutline} />
            <SidebarMenuItem text="List" Icon={ListOutline} />
            <SidebarMenuItem text="Profile" Icon={ProfileOutline} />
            <SidebarMenuItem text="More" Icon={MoreOutline} />
          </>
        )}
      </div>

      {/* Button */}
      {currentUser ? (
        <>
          <button className="bg-blue-400 text-white rounded-full w-56 h-12 font-bold shadow-md hover:brightness-95 text-lg hidden xl:inline">
            Tweet
          </button>
          {/* Mini Profile */}
          <div className="hoverEffect text-gray-700 flex items-center justify-center xl:justify-start mt-auto">
            <img
              src={currentUser?.avatar || "https://i.imgur.com/6VBx3io.png"}
              alt="sokea"
              className="w-10 h-10 rounded-full xl:mr-2 object-cover"
            />

            <div className="leading-5 hidden xl:inline ">
              <h3 className="font-bold ">{currentUser?.name || "Unknown"}</h3>
              <p className="text-gray-500 ">
                {currentUser?.username || "Unknown"}
              </p>
            </div>
            <DotHorizontal className={"h-5 xl:ml-8 hidden xl:inline"} />
          </div>
        </>
      ) : (
        <button
          onClick={() => router.push("/auth/signin")}
          className="bg-blue-400 text-white rounded-full w-36 h-12 font-bold shadow-md hover:brightness-95 text-lg hidden xl:inline"
        >
          Sign in
        </button>
      )}
    </div>
  );
}
